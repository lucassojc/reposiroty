/*
 Navicat Premium Data Transfer

 Source Server         : projekat
 Source Server Type    : MySQL
 Source Server Version : 100131
 Source Host           : localhost:3306
 Source Schema         : project

 Target Server Type    : MySQL
 Target Server Version : 100131
 File Encoding         : 65001

 Date: 12/05/2018 15:03:50
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for administrator
-- ----------------------------
DROP TABLE IF EXISTS `administrator`;
CREATE TABLE `administrator`  (
  `administrator_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `username` varchar(128) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `password` varchar(128) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `is_active` tinyblob NOT NULL,
  PRIMARY KEY (`administrator_id`) USING BTREE,
  UNIQUE INDEX `uq_username`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of administrator
-- ----------------------------
INSERT INTO `administrator` VALUES (1, '0000-00-00 00:00:00', 'admin', 'admin', 0x31);
INSERT INTO `administrator` VALUES (2, '2018-04-15 22:00:38', 'admin1', 'admin1', 0x31);

-- ----------------------------
-- Table structure for cart
-- ----------------------------
DROP TABLE IF EXISTS `cart`;
CREATE TABLE `cart`  (
  `cart_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `session_number` int(11) NOT NULL,
  PRIMARY KEY (`cart_id`) USING BTREE,
  UNIQUE INDEX `uq_session_number`(`session_number`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for cart_product
-- ----------------------------
DROP TABLE IF EXISTS `cart_product`;
CREATE TABLE `cart_product`  (
  `cart_product_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `added_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `amount` decimal(10, 2) NOT NULL,
  `cart_id` int(11) UNSIGNED NOT NULL,
  `product_id` int(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`cart_product_id`) USING BTREE,
  INDEX `fk_cart_product_product_id`(`product_id`) USING BTREE,
  INDEX `fk_cart_product_cart_id`(`cart_id`) USING BTREE,
  CONSTRAINT `fk_cart_product_cart_id` FOREIGN KEY (`cart_id`) REFERENCES `cart` (`cart_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_cart_product_product_id` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for category
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category`  (
  `category_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `name` varchar(128) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `description` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `image` varchar(128) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `administrator_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`category_id`) USING BTREE,
  INDEX `fk_category_administrator_id`(`administrator_id`) USING BTREE,
  CONSTRAINT `fk_category_administrator_id` FOREIGN KEY (`administrator_id`) REFERENCES `administrator` (`administrator_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = latin2 COLLATE = latin2_bin ROW_FORMAT = Compact;

-- ----------------------------
-- Records of category
-- ----------------------------
INSERT INTO `category` VALUES (1, '0000-00-00 00:00:00', 'Televizori', 'Ovo su televizori.', 'slika1', 1);
INSERT INTO `category` VALUES (2, '0000-00-00 00:00:00', 'Mobilni telefoni', 'Ovo su mobilni telefoni', 'slika2', 1);
INSERT INTO `category` VALUES (3, '2018-04-17 13:22:39', 'Kompijuteri', 'Ovo su kompijuteri', 'slika3', 1);
INSERT INTO `category` VALUES (4, '2018-05-12 14:54:30', 'Foto oprema', 'Foto aparati i kamere', 'slika4', 2);

-- ----------------------------
-- Table structure for order
-- ----------------------------
DROP TABLE IF EXISTS `order`;
CREATE TABLE `order`  (
  `order_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `customer_first_name` varchar(128) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `customer_last_name` varchar(128) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `customer_email` varchar(128) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `customer_adress` varchar(128) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `order_status` varchar(128) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `cart_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`order_id`) USING BTREE,
  INDEX `fk_order_cart_id`(`cart_id`) USING BTREE,
  CONSTRAINT `fk_order_cart_id` FOREIGN KEY (`cart_id`) REFERENCES `cart` (`cart_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for product
-- ----------------------------
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product`  (
  `product_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `title` varchar(128) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `description` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `image` varchar(128) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `price` decimal(10, 2) NOT NULL,
  `administrator_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`product_id`) USING BTREE,
  UNIQUE INDEX `uk_image`(`image`) USING BTREE,
  INDEX `fk_product_administrator_id`(`administrator_id`) USING BTREE,
  INDEX `fk_product_category_id`(`category_id`) USING BTREE,
  CONSTRAINT `fk_product_administrator_id` FOREIGN KEY (`administrator_id`) REFERENCES `administrator` (`administrator_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_product_category_id` FOREIGN KEY (`category_id`) REFERENCES `category` (`category_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of product
-- ----------------------------
INSERT INTO `product` VALUES (1, '2018-04-15 21:42:17', 'telefon1', 'Ovo je neki telefon', '/Projekat/assets/products/1.png', 10000.00, 1, 2);
INSERT INTO `product` VALUES (2, '0000-00-00 00:00:00', 'telefon2', 'ovo je neki telefon', '/Projekat/assets/products/2.png', 20000.00, 1, 2);
INSERT INTO `product` VALUES (3, '0000-00-00 00:00:00', 'televizor1', 'ovo je televiyor 1', '/Projekat/assets/products/3.png', 50000.00, 1, 1);
INSERT INTO `product` VALUES (4, '0000-00-00 00:00:00', 'televizor2', 'dasoodasdoadasod', '/Projekat/assets/products/4.png', 100000.00, 1, 1);
INSERT INTO `product` VALUES (5, '2018-05-12 14:55:09', 'kompijuter1', 'ovo je neki kompijuter', '/Projekat/assets/products/5.png', 35000.00, 2, 3);
INSERT INTO `product` VALUES (7, '2018-05-12 14:56:11', 'kompijuter2', 'ovo je neki kompijuter bla bla', '/Projekat/assets/products/6.png', 60000.00, 2, 3);
INSERT INTO `product` VALUES (8, '2018-05-12 14:56:41', 'Fotoaparat', 'ovo je neki foroaparat', '/Projekat/assets/products/7.png', 50000.00, 1, 4);
INSERT INTO `product` VALUES (9, '2018-05-12 14:57:29', 'Kamera', 'Ovo je neka kamera ', '/Projekat/assets/products/8.png', 70000.00, 2, 4);

-- ----------------------------
-- Table structure for product_view
-- ----------------------------
DROP TABLE IF EXISTS `product_view`;
CREATE TABLE `product_view`  (
  `product_view_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `product_id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(14) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `user_agent` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`product_view_id`) USING BTREE,
  INDEX `fk_product_view_product_id`(`product_id`) USING BTREE,
  INDEX `auction_view_ip_address_idx`(`ip_address`) USING BTREE,
  CONSTRAINT `fk_product_view_product_id` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of product_view
-- ----------------------------
INSERT INTO `product_view` VALUES (1, '2018-05-12 14:57:49', 9, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0');
INSERT INTO `product_view` VALUES (2, '2018-05-12 14:59:28', 9, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0');
INSERT INTO `product_view` VALUES (3, '2018-05-12 15:03:30', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0');

SET FOREIGN_KEY_CHECKS = 1;
