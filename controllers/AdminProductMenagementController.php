<?php
    namespace App\Controllers;

    use App\Models\ProductModel;
    use App\Models\CategoryModel;

    class AdminProductMenagementController extends \App\Core\Role\AdminRoleController {
        public function products() {
            /*$categoryModel = new CategoryModel($this->getDatabaseConnection());
            $categories = $categoryModel->getAll();
            $this->set('categories', $categories);*/

            $productModel = new ProductModel($this->getDatabaseConnection());
            $products = $productModel->getAll();
            $this->set('products', $products);
        }

        public function getEdit($productId) {
            $productModel = new ProductModel($this->getDatabaseConnection());
            $product = $productModel->getById($productId);

            if (!$product) {
                $this->redirect(\Configuration::BASE . 'admin/products');
            }

            $this->set('product', $product);

            return $productModel;
            
        }

        public function postEdit($productId) {
            $productModel = $this->getEdit($productId);

            $title = \filter_input(INPUT_POST, 'title', FILTER_SANITIZE_STRING);
            $description = \filter_input(INPUT_POST, 'description', FILTER_SANITIZE_STRING);
            $price = \filter_input(INPUT_POST, 'price', FILTER_SANITIZE_STRING);

            $productModel->editById($productId, [
                'title' => $title,
                'description' => $description,
                'price' => $price
            ]);

            $uploadStatus = $this->doImageUpload('image', $productId, '.png');
            if (!$uploadStatus) {
                $this->set('message', 'Proizvod je izmenjen, ali nije dodata nova slika.');
                return;
            }

            $this->redirect(\Configuration::BASE . 'admin/products');
        }

        public function getAdd() {
            $categoryModel = new CategoryModel($this->getDatabaseConnection());
            $categories = $categoryModel->getAll();
            $this->set('categories', $categories);
        }

        public function postAdd() {
            $title = \filter_input(INPUT_POST, 'title', FILTER_SANITIZE_STRING);
            $description = \filter_input(INPUT_POST, 'description', FILTER_SANITIZE_STRING);
            $price = \filter_input(INPUT_POST, 'price', FILTER_SANITIZE_STRING);
            $administratorId =  $this->getSession()->get('administrator_id');
            $categoryId = \filter_input(INPUT_POST, 'category_id', FILTER_SANITIZE_STRING);

            $productModel = new ProductModel($this->getDatabaseConnection());

            $productId = $productModel->add([
                'title' => $title,
                'description' => $description,
                'price' => $price,
                'category_id' => $categoryId,
                'administrator_id' => $administratorId
            ]);

            print_r($productId);

            $uploadStatus = $this->doImageUpload('image', $productId, '.png');
            if (!$uploadStatus) {
                return;
            }

            if ($productId) {
                $this->redirect(\Configuration::BASE . 'admin/products');
            }

            $this->set('message', 'Došlo je do greške: Nije moguće dodati ovaj proizvod.');
        }

        private function doImageUpload (string $fieldName, string $fileName): bool {
            unlink(\Configuration::UPLOAD_DIR . $fileName . '.png');

            $storage = new \Upload\Storage\FileSystem(\Configuration::UPLOAD_DIR);
            $file = new \Upload\File($fieldName, $storage);
            $file->setName($fileName);
            $file->addValidations([
                new \Upload\Validation\Mimetype('image/png'),
                new \Upload\Validation\Size("3M")
            ]);

            try {
                $file->upload();
                return true;
            } catch (\Exception $e) {
                $this->set('message', 'Greska: ' . $e->getMessage());
                return false;
            }
        }
    }