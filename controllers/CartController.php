<?php
    namespace App\Controllers;

    use App\Core\ApiController;
    use App\Models\CartModel;
    use App\Models\ProductModel;
    use App\Models\CartProductModel;
    use App\Core\Fingerprint\FingerprintProvider;


    class CartController extends ApiController {
        public function checkout() {

            
            $items = $this->getSession()->get('items', []);
            $this->set('items', $items);

            $cartModel = new CartModel($this->getDatabaseConnection());

            $sessionNumber = $this->getSession()->get('__fingerprint', '');

            $cartModel->add(
                [
                    'session_number' => $sessionNumber
                ]    
            );
            #TODO : Implement.

            $cartProductModel = new CartProductModel($this->getDatabaseConnection());

            $amount = count($items);

            $productModel = new ProductModel($this->getDatabaseConnection());
            //$productId = $productModel->getById();

           // $cartId = $cartModel->getById();

            $cartProductModel->add(
                [
                    'amount'     => $amount,
                    //'cart_id'    => $cartId,
                    //'product_id' => $productId
                ]
            );
        }
    }