<?php
    namespace App\Controllers;

    use App\Core\Controller;
    use App\Models\OrderModel;

    class OrderController extends Controller {
        public function getAdd() {
        }

        public function postAdd() {
            $customerFirstName = \filter_input(INPUT_POST, 'customer_first_name', FILTER_SANITIZE_STRING);
            $customerLastName = \filter_input(INPUT_POST, 'customer_last_name', FILTER_SANITIZE_STRING);
            $customerEmail = \filter_input(INPUT_POST, 'customer_email', FILTER_SANITIZE_STRING);
            $customerAddress = \filter_input(INPUT_POST, 'customer_address', FILTER_SANITIZE_STRING);
            //$orderStatus =  
            //$cartId = 

            $orderModel = new OrderModel($this->getDatabaseConnection());

            $orderId = $orderModel->add([
                'customer_first_name' => $customerFirstName,
                'customer_last_name' => $customerLastName,
                'customer_email' => $customerEmail,
                'customer_address' => $customerAddress,
                //'order_status' => $orderStatus,
                //'cart_id' => $cartId
            ]);

            if (!$orderId) {
                $this->set('message', 'Došlo je do greške: Podaci nisu ispravni.');
            }
        }
    }