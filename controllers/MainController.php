<?php
    namespace App\Controllers;

    use App\Models\CategoryModel;
    use App\Models\ProductViewModel;
    use App\Core\Controller;

    class MainController extends Controller {
        public function home() {
            $categoryModel = new CategoryModel($this->getDatabaseConnection());
            $categories = $categoryModel->getAll();
            $this->set('categories', $categories);

            $staraVrednost = $this->getSession()->get('brojac', 0);
            $novaVrednost = $staraVrednost + 1;
            $this->getSession()->put('brojac', $novaVrednost);
            $this->set('podatak', $novaVrednost);
        }

        public function getLogin() {

        }

        public function postLogin() {
            $username = \filter_input(INPUT_POST, 'login_username', FILTER_SANITIZE_STRING);
            $password = \filter_input(INPUT_POST, 'login_password', FILTER_SANITIZE_STRING);

            $validanPassword = (new \App\Validators\StringValidator())
                ->setMinLength(4)
                ->setMaxLength(120)
                ->isValid($password);
            
            if (!$validanPassword) {
                $this->set('message', 'Došlo je do greške, lozinka nije validna.');
                return;
            }

            $administratorModel = new \App\Models\AdministratorModel($this->getDatabaseConnection());

            $admin = $administratorModel->getByFieldName('username', $username);
            if(!$admin) {
                $this->set('message', 'Došlo je do greške: Ne postoji korisnik sa tim korisničkim imenom.');
                return;
            }

            $isPassword = $administratorModel->getByFieldName('password', $password);
            if (!$isPassword) {
                sleep(1);
                $this->set('message', 'Došlo je do greške: Lozinka nije ispravna.');
                return;
            }

            $this->getSession()->put('administrator_id', $admin->administrator_id);
            $this->getSession()->save();
            $this->redirect(\Configuration::BASE . 'admin-panel');


        }

        public function logout() {
            if($this->getSession()->get('administrator_id') === null) {
                $this->redirect(\Configuration::BASE . 'admin/login');
            }

            $this->getSession()->put('administrator_id', null);
        }
    }