<?php
    namespace App\Controllers;

    use App\Core\ApiController;
    use App\Models\ProductModel;

    class ApiCartController extends ApiController {
        public function getItems() {
            $items = $this->getSession()->get('items', []);
            $this->set('items', $items);
        }

        public function addCart($productId) {
            $productModel = new ProductModel($this->getDatabaseConnection());
            $product = $productModel->getById($productId);

            if(!$product) {
                $this->set('error', -1);
                return;
            }

            $items = $this->getSession()->get('items', []);

            foreach ($items as $cart) {
                if ($cart->product_id == $productId) {
                    $this->set('error', -2);
                    return;
                }
            }
            
            $items[] = $product;
            $this->getSession()->put('items', $items);

            $this->set('error', 0);
            return;
        }

        public function clear() {
            $this->getSession()->put('items', []);
            
            $this->set('error', 0);
        }
    }