function getItems() {
    fetch(BASE + 'api/items', { credentials: 'include' })
        .then(result => result.json())
        .then(data => {
            displayItems(data.items);
        });
}

function addCart(productId) {
    fetch(BASE + 'api/items/add/' + productId, { credentials: 'include' })
        .then(result => result.json())
        .then(data => {
            if (data.error === 0) {
                getItems();
            }
        });
}

function clearItems() {
    fetch(BASE + 'api/items/clear', { credentials: 'include' })
        .then(result => result.json())
        .then(data => {
            if (data.error === 0) {
                getItems();
            }
        });
}

function displayItems(items) {
    const itemsDiv = document.querySelector('.items');
    itemsDiv.innerHTML = '';

    if (items.length === 0) {
        itemsDiv.innerHTML = 'KORPA JE PRAZNA!';
        return;
    }

    for (cart of items) {
        const cartLink = document.createElement('a');
        cartLink.style.display = 'block';
        cartLink.innerHTML = cart.title;
        cartLink.href = BASE + 'product/' + cart.product_id;

        itemsDiv.appendChild(cartLink);
    }
}

addEventListener('load', getItems);